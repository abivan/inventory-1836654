INSERT INTO users(email, last_name, name, password, username, enabled) VALUES ('abivan@correo.com','agudelo', 'ivan', '$2a$10$4d4oANPzXqHE2iG29ZHGT.ujMvu9TFh.QE4YES1j2QPo7Pw60kZUW', 'ivan', true);
INSERT INTO users(email, last_name, name, password, username, enabled) VALUES ('fabian@correo.com','rojas', 'fabian', '$2a$10$bV2lishgqhdF/yQhGyJOZOn3o4XQwb235jWgqcYNtXeGO3D3eKI8K', 'fabian', true);
INSERT INTO users(email, last_name, name, password, username, enabled) VALUES ('yohon@correo.com','bravo', 'yohon', '$2a$10$bV2lishgqhdF/yQhGyJOZOn3o4XQwb235jWgqcYNtXeGO3D3eKI8K', 'yohon', true);
INSERT INTO users(email, last_name, name, password, username, enabled) VALUES ('freddy@correo.com','sierra', 'freddy', '$2a$10$bV2lishgqhdF/yQhGyJOZOn3o4XQwb235jWgqcYNtXeGO3D3eKI8K', 'freddy', true);

INSERT INTO rols VALUES (1,'ROLE_USER');
INSERT INTO rols VALUES (2,'ROLE_ADMIN');

INSERT INTO user_has_rol VALUES (1,1), (1,2);
INSERT INTO user_has_rol VALUES (2,1);
INSERT INTO user_has_rol VALUES (3,2);

INSERT INTO product(reference, name) VALUES ('1', 'Teclado'), ('2', 'Portatil');

