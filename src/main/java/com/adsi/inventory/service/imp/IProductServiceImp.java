package com.adsi.inventory.service.imp;

import com.adsi.inventory.domain.Product;
import com.adsi.inventory.repository.ProductRepository;
import com.adsi.inventory.service.IProductService;
import com.adsi.inventory.service.dto.ProductDTO;
import com.adsi.inventory.service.dto.ProductTransformer;
import com.adsi.inventory.service.error.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IProductServiceImp implements IProductService {

    @Autowired
    ProductRepository repository;

    @Override
    public Page<ProductDTO> getAll(Integer pageNumber, Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return repository.findAll(pageable)
                .map(ProductTransformer::getProductDTOFromProduct);
    }

    @Override
    public ProductDTO create(ProductDTO productDTO) {
        return ProductTransformer.getProductDTOFromProduct(repository.save(ProductTransformer.getProductFromProductDTO(productDTO)));
    }

    @Override
    public ProductDTO getById(String reference) {
        Optional<Product> product = repository.findById(reference);
        if (!product.isPresent()) {
            throw new ObjectNotFoundException("error: el producto con referencia = " + reference + " no existe");
        }
        return product
                .map(ProductTransformer::getProductDTOFromProduct)
                .get();
    }
}
